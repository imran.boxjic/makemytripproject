create schema makemytrip


create table login_details (
id int not null auto_increment,
firstname varchar(100) not null,
lastname varchar(100) not null,
username varchar(100) not null,
email varchar(100) not null,
password varchar(100) not null,
phone varchar(10) not null,
primary key (id)
)

CREATE TABLE `flightlist` (
  `id` int NOT NULL AUTO_INCREMENT,
  `flightname` varchar(45) DEFAULT NULL,
  `departuretime` varchar(10) DEFAULT NULL,
  `arrivaltime` varchar(10) DEFAULT NULL,
  `departuredate` varchar(10) DEFAULT NULL,
  `arrivaldate` varchar(10) DEFAULT NULL,
  `price` int DEFAULT NULL,
  `baggage` varchar(4) DEFAULT NULL,
  `from` varchar(100) DEFAULT NULL,
  `to` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) 

CREATE TABLE `selection` (
  `id` int NOT NULL AUTO_INCREMENT,
  `route` varchar(45) DEFAULT NULL,
  `from` varchar(100) DEFAULT NULL,
  `to` varchar(100) DEFAULT NULL,
  `departuredate` varchar(10) DEFAULT NULL,
  `arrivaldate` varchar(10) DEFAULT NULL,
  `class` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
)

INSERT INTO `makemytrip`.`flightlist` (`id`, `flightname`, `departuretime`, `arrivaltime`, `departuredate`, `arrivaldate`, `price`, `baggage`, `from`, `to`) VALUES ('1', 'Indigo', '03:05', '13:15', '20-03-2020', '20-03-2020', '6500', '10kg', 'Chennai', 'Madurai');
INSERT INTO `makemytrip`.`flightlist` (`id`, `flightname`, `departuretime`, `arrivaltime`, `departuredate`, `arrivaldate`, `price`, `baggage`, `from`, `to`) VALUES ('2', 'AirIndia', '15:12', '22:20', '23-03-2020', '23-03-2020', '8500', '15kg', 'Delhi', 'Myanmar');
INSERT INTO `makemytrip`.`flightlist` (`id`, `flightname`, `departuretime`, `arrivaltime`, `departuredate`, `arrivaldate`, `price`, `baggage`, `from`, `to`) VALUES ('3', 'QatarAirways', '21:00', '10:00', '25-03-2020', '26-03-2020', '10500', '20kg', 'Qatar', 'India');
INSERT INTO `makemytrip`.`flightlist` (`id`, `flightname`, `departuretime`, `arrivaltime`, `departuredate`, `arrivaldate`, `price`, `baggage`, `from`, `to`) VALUES ('4', 'QatarAirways', '21:00', '10:00', '25-03-2020', '26-03-2020', '10500', '20kg', 'India', 'Kuwait');
INSERT INTO `makemytrip`.`flightlist` (`id`, `flightname`, `departuretime`, `arrivaltime`, `departuredate`, `arrivaldate`, `price`, `baggage`, `from`, `to`) VALUES ('5', 'AirIndia', '15:12', '22:20', '23-03-2020', '23-03-2020', '8500', '15kg', 'Chennai', 'Coimbatore');
INSERT INTO `makemytrip`.`flightlist` (`id`, `flightname`, `departuretime`, `arrivaltime`, `departuredate`, `arrivaldate`, `price`, `baggage`, `from`, `to`) VALUES ('6', 'Indigo', '03:05', '13:15', '20-03-2020', '20-03-2020', '6500', '10kg', 'Coimbatore', 'Trichy');
INSERT INTO `makemytrip`.`flightlist` (`id`, `flightname`, `departuretime`, `arrivaltime`, `departuredate`, `arrivaldate`, `price`, `baggage`, `from`, `to`) VALUES ('7', 'Indigo', '10:00', '16:00', '21-03-2020', '21-03-2020', '6500', '10kg', 'Madurai', 'Chennai');
INSERT INTO `makemytrip`.`flightlist` (`id`, `flightname`, `departuretime`, `arrivaltime`, `departuredate`, `arrivaldate`, `price`, `baggage`, `from`, `to`) VALUES ('8', 'AirIndia', '16:00', '23:00', '24-03-2020', '24-03-2020', '8500', '15kg', 'Myanmar', 'Delhi');
INSERT INTO `makemytrip`.`flightlist` (`id`, `flightname`, `departuretime`, `arrivaltime`, `departuredate`, `arrivaldate`, `price`, `baggage`, `from`, `to`) VALUES ('9', 'QatarAirways', '01:00', '14:00', '26-03-2020', '26-03-2020', '10500', '20kg', 'India', 'Qatar');
INSERT INTO `makemytrip`.`flightlist` (`id`, `flightname`, `departuretime`, `arrivaltime`, `departuredate`, `arrivaldate`, `price`, `baggage`, `from`, `to`) VALUES ('10', 'QatarAirways', '05:00', '13:00', '26-03-2020', '26-03-2020', '10500', '20kg', 'Kuwait', 'India');
INSERT INTO `makemytrip`.`flightlist` (`id`, `flightname`, `departuretime`, `arrivaltime`, `departuredate`, `arrivaldate`, `price`, `baggage`, `from`, `to`) VALUES ('11', 'AirIndia', '20:00', '06:00', '24-03-2020', '25-03-2020', '8500', '15kg', 'Coimbatore', 'Chennai');
INSERT INTO `makemytrip`.`flightlist` (`id`, `flightname`, `departuretime`, `arrivaltime`, `departuredate`, `arrivaldate`, `price`, `baggage`, `from`, `to`) VALUES ('12', 'Indigo', '10:00', '13:05', '21-03-2020', '21-03-2020', '6500', '10kg', 'Trichy', 'Coimbatore');

