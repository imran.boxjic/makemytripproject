<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="css/myapp.css">
<link rel="stylesheet" href="css/slideshow.css">
<title>Sign Up</title>
</head>
<body>
<body id="back1">
	<div class="navigation">
		<div class="navigation-left">
			<a href="launch.jsp"><button class="login-btn">Home</button></a>
		</div>
		<div class="navigation-center">
			<a href="launch.jsp"><img src="images/logo.png" alt=""></a>
		</div>
	</div>
	<div class="signupmain">
		<p class="sign" align="center">Sign Up</p>
		<form class="form1" action="newuser" method="post">
			<input class="un" type="text" name="first" align="center"
				placeholder="First Name"> 
				<input class="un" type="text"
				name="last" align="center" placeholder="Last Name"> 
				<input class="un" type="text"
				name="username" align="center" placeholder="User Name"> 
				<input
				class="un" type="text" name="email" align="center"
				placeholder="E-Mail">
				<input
				class="pass" name="pass" type="password" align="center"
				placeholder="Password"> 
				<input class="un" name="phone"
				type="tel" align="center" placeholder="Phone Number"> 
				<input
				type=submit class="submit2" align="center" value="Create Account">
		</form>
	</div>
</html>