
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Flight - Travel and Tour</title>
<!-- <script src="script/jquery-1.12.1.js"></script>
<script src="script/jquery.1.12.4.js"></script> -->
<!-- <script type="text/javascript" src="script/jquery-ui.min.js" charset="utf-8"></script>
<script type="text/javascript" src="script/jquery.min.js" charset="utf-8"></script> -->
<script type="text/javascript"
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<script type="text/javascript"
	src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.11/jquery-ui.min.js"></script>

<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
<script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<link rel="stylesheet" href="css/jquery-ui.css">
<link rel="stylesheet" href="css/radio.css">
<script>

	$(function() {
		
		$("#datepicker").datepicker();
		$("#datepicker1").datepicker();
		
		$("#autocomplete").autocomplete({
			source: ${jsonlocations}
		});
		
		$("#autocomplete1").autocomplete({
			source: ${jsonlocations}
		});
		
		$( "#spinner-1" ).spinner({  
			step: 1,
			min: 1,   
            max: 8  
         });  
	});
	
	function yesnoCheck() {
	    if (document.getElementById('yesCheck').checked) {
	        document.getElementById('ifYes').style.display = 'block';
	    }
	    else document.getElementById('ifYes').style.display = 'none';

	}
</script>
</head>
<%

if(session.getAttribute("user")==null)
	response.sendRedirect("signin");
%>
<body id="back1">
	<jsp:include page="navbar.jsp" />

	<div class="booking">
		<p class="sign" align="center">Check Flight Availability</p>
		<form class="form1" action="/makemytrip/NewUser" method="post">
			<div>
				<div class="rad">
					<label class="container" class="radiodiv">OneWay<input
						type="radio" id="noCheck" checked="checked" name="trip"
						onclick="javascript:yesnoCheck();"> <span
						class="checkmark"></span>
					</label> <label class="container" class="radiodiv">Round Trip <input
						type="radio" id="yesCheck" name="trip"
						onclick="javascript:yesnoCheck();"> <span
						class="checkmark"></span>
					</label>
				</div>
				<input type="text" name="from" class="un" id="autocomplete"
					placeholder="Enter Departure Location"> <input type="text"
					name="to" class="un" id="autocomplete1"
					placeholder="Enter Arrival Location"> <input type="text"
					name="departure" class="un" id="datepicker"
					placeholder="Departure Date">

				<div id="ifYes" style="display: none">
					<input type="text" name="return" class="un" id="datepicker1"
						placeholder="Returning Date" />
				</div>
				
				<div class="un">
					<input name="adults" id="spinner-1" align="center" placeholder="No of Passengers">
				</div>

				<div class="rad">
					<label class="container" class="radiodiv">Economy<input
						type="radio" id="noCheck" checked="checked" name="class"
						onclick="javascript:yesnoCheck();"> <span
						class="checkmark"></span>
					</label> <label class="container" class="radiodiv">Business<input
						type="radio" id="yesCheck" name="class"
						onclick="javascript:yesnoCheck();"> <span
						class="checkmark"></span>
					</label>
				</div>

				<input type=submit class="submit2" align="center"
					value="Check Flight Availability">
			</div>
		</form>
	</div>
</body>
</html>