<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="css/slideshow.css">
<script type="text/javascript" src="script/myapp.js"></script>
<title>:: Welcome to Make My Trip ::</title>
</head>
<body>
	<div class="navigation">
		<div class="navigation-left">
			<a href="signup"><button class="login-btn">Sign Up</button></a>
		</div>
		<div class="navigation-center">
			<a href="launch"><img src="images/logo.png" alt=""></a>
		</div>
		<div class="navigation-right">
			<a href="signin"><button class="login-btn">Sign In</button></a>
		</div>
	</div>
	<!-- Slider Wrapper -->
	<div class="css-slider-wrapper">
		<input type="radio" name="slider" class="slide-radio1" checked
			id="slider_1"> <input type="radio" name="slider"
			class="slide-radio2" id="slider_2"> <input type="radio"
			name="slider" class="slide-radio3" id="slider_3"> <input
			type="radio" name="slider" class="slide-radio4" id="slider_4">

		<!-- Slider Pagination -->
		<div class="slider-pagination">
			<label for="slider_1" class="page1"></label> <label for="slider_2"
				class="page2"></label> <label for="slider_3" class="page3"></label>
			<label for="slider_4" class="page4"></label>
		</div>

		<!-- Slider #1 -->
		<div class="slider slide-1">
			<img src="images/flight3.png" alt="">
			<div class="slider-content">
				<h4>Join Us</h4>
				<h2>Get Hassle Free Services</h2>
				<a href="home.jsp"><button type="button" class="buy-now-btn" name="button">Book
					Now</button></a>
			</div>
			<div class="number-pagination">
				<span>1</span>
			</div>
		</div>

		<!-- Slider #2 -->
		<div class="slider slide-2">
			<img src="images/flight2.png" alt="">
			<div class="slider-content">
				<h4>Join Us</h4>
				<h2>Book Tickets from Anywhere And Anytime</h2>
				<a href="home.jsp"><button type="button" class="buy-now-btn" name="button">Book
					Now</button></a>
			</div>
			<div class="number-pagination">
				<span>2</span>
			</div>
		</div>

		<!-- Slider #3 -->
		<div class="slider slide-3">
			<img src="images/flight1.png" alt="">
			<div class="slider-content">
				<h4>Join Us</h4>
				<h2>Travel The World With Us</h2>
				<a href="home.jsp"><button type="button" class="buy-now-btn" name="button">Book
					Now</button></a>
			</div>
			<div class="number-pagination">
				<span>3</span>
			</div>
		</div>

		<!-- Slider #4 -->
		<div class="slider slide-4">
			<img src="images/flight4.png" alt="">
			<div class="slider-content">
				<h4>Join Us</h4>
				<h2>Flight Ticket Booking Made Easy Through Online</h2>
				<a href="home.jsp"><button type="button" class="buy-now-btn" name="button">Book
					Now</button></a>
			</div>
			<div class="number-pagination">
				<span>4</span>
			</div>
		</div>
	</div>
	<script src="script/jquery.js" charset="utf-8"></script>
	<script>
		var TIMEOUT = 2100;
		var interval = setInterval(handleNext, TIMEOUT);
	</script>
</body>
</html>