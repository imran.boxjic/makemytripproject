<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="css/myapp.css">
<link rel="stylesheet" href="css/slideshow.css">
<title>:: Welcome to Make My Trip ::</title>
</head>
<body id="back1">
	<div class="navigation">
		<div class="navigation-left">
			<a href="launch" class="login-btn">Home</a>
		</div>
		<div class="navigation-center">
			<a href="launch"><img src="images/logo.png" alt=""></a>
		</div>
	</div>
	<c:if test="${not empty error}">
   Error: ${error}
</c:if>
	<div class="main">
		<p class="sign" align="center">Sign in</p>
		<form class="form1" action="loging" method="post">
			<input class="un " type="text" name="user" align="center"
				placeholder="Username"> <input class="pass" name="pass"
				type="password" align="center" placeholder="Password"> <input
				type=submit class="submit" align="center" value="Sign In">
		</form>
		<p class="createnew" align="center">
			<a href="signup">Create New Account</a>
		</p>
	</div>
</body>
</html>