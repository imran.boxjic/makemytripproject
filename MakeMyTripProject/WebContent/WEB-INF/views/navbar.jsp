<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<link rel="stylesheet" href="css/myapp.css">

<link rel="stylesheet" href="css/navigationdrop.css">

<div class="navbar">
	<a href="loginhome"><button class="login-btn">Home</button></a> <a href="#news"><img src="images/logo.png" alt=""></a>
	<a href="#news"><img src="images/support_logo.png" alt=""></a>
	<div class="dropdown">
		<button class="dropbtn" onclick="myFunction()">
			Hello, Mr. ${sessionScope.user} <i class="fa fa-caret-down"></i>
		</button>
		<div class="dropdown-content" id="myDropdown">
			<a href="home.jsp" class="dropbtn">My Trips</a>
			<a href="#" class="dropbtn">My Profile</a>
			<a href="logout" class="dropbtn">Log Out</a>
		</div>
	</div>
</div>

<script>
	/* When the user clicks on the button, 
	 toggle between hiding and showing the dropdown content */
	function myFunction() {
		document.getElementById("myDropdown").classList.toggle("show");
	}

	// Close the dropdown if the user clicks outside of it
	window.onclick = function(e) {
		if (!e.target.matches('.dropbtn')) {
			var myDropdown = document.getElementById("myDropdown");
			if (myDropdown.classList.contains('show')) {
				myDropdown.classList.remove('show');
			}
		}
	}
</script>
