package com.imr.utility;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;

public class Utility {

	private ArrayList<String> locations = new ArrayList<String>();

	public ArrayList<String> getLocations() {
		try {

			String fileName = "config/locations.txt";
			ClassLoader classLoader = new Utility().getClass().getClassLoader();
			File file = new File(classLoader.getResource(fileName).getFile());
			FileReader fr = new FileReader(file);

			BufferedReader brr = new BufferedReader(fr);
			String line = "";
			while ((line = brr.readLine()) != null) {
				locations.add(line.toUpperCase());
			}
			brr.close();
			return locations;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
