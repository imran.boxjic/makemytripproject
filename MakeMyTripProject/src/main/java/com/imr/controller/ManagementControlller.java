package com.imr.controller;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.imr.dao.LoginDAO;
import com.imr.utility.Utility;

@Controller
public class ManagementControlller {

	@Autowired
	Utility util;
	
	@Autowired
	LoginDAO logindao;

	@RequestMapping("/")
	public ModelAndView welcome() {

		return new ModelAndView("launch");
	}

	@RequestMapping("/signup")
	public ModelAndView signup() {

		return new ModelAndView("signup");
	}

	@RequestMapping("/signin")
	public ModelAndView signin() {

		return new ModelAndView("home");
	}

	@RequestMapping("/launch")
	public ModelAndView launch() {

		return new ModelAndView("launch");
	}

	@RequestMapping("/loginhome")
	public ModelAndView loginhome(ModelMap model) {

		return new ModelAndView("login");
	}

	@RequestMapping("/logout")
	public ModelAndView loginout(HttpServletRequest request) {

		request.getSession().invalidate();
		return new ModelAndView("launch");
	}

	@PostMapping("/newuser")
	public ModelAndView newuser(@RequestParam("first") String first, @RequestParam("last") String last,
			@RequestParam("username") String username, @RequestParam("email") String email,
			@RequestParam("pass") String pass, @RequestParam("phone") String phone, HttpServletRequest request) {

		String result = logindao.saveToDB(first, last, username, email, pass, phone);
		
		return new ModelAndView("home");
	}

	@PostMapping("/loging")
	public ModelAndView login(@RequestParam("user") String user, @RequestParam("pass") String pass,
			HttpServletRequest request, ModelMap model) {

		LoginDAO l = new LoginDAO();
		int result = l.checkLoginDetails(user, pass);
		if (result == 1) {

			ArrayList<String> a = util.getLocations();
			// model.addAttribute("user", user.toUpperCase());
			request.getSession().setAttribute("user", user.toUpperCase());
			model.addAttribute("locationarray", a);
			GsonBuilder gsonBuilder = new GsonBuilder();
			Gson gson = gsonBuilder.create();
			String JSONObject = gson.toJson(a);

			model.addAttribute("jsonlocations", JSONObject);

			return new ModelAndView("login");
		} else {
			ModelAndView mv = new ModelAndView("home");
			mv.addObject("message", "Invalid Credentials");
			return new ModelAndView("home");
		}
	}
	
	@PostMapping("/availability")
	public ModelAndView availability(@RequestParam("from") String first, @RequestParam("to") String last,
			@RequestParam("trip") String username, @RequestParam("departure") String email,
			@RequestParam("pass") String pass, @RequestParam("phone") String phone, HttpServletRequest request) {

		String result = logindao.saveToDB(first, last, username, email, pass, phone);
		
		return new ModelAndView("home");
	}
}
